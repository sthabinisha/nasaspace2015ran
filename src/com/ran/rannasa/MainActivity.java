package com.ran.rannasa;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.ran.rannasa.parser.ServiceHandler;

public class MainActivity extends ListActivity {
	private ProgressDialog pDialog;

	// URL to get JSON
	private static String url = "http://nasa.toletnepal.com/json.php";

	// JSON Node names
	private static final String TAG_CURRENT = "Current";
	private static final String TAG_POWER = "Power";
	private static final String TAG_UNIT = "Unit";
	private static final String TAG_PRICE = "Price";
	JSONArray detail;
	ListView lview;

Button refresh;
	ArrayList<HashMap<String, String>> detailList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// lview = (ListView) findViewById(R.id.list);
		refresh = (Button) findViewById(R.id.Refresh);
		refresh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				
			}
		});

		populateList();

	}

	private void populateList() {
		// TODO Auto-generated method stub
		detailList = new ArrayList<HashMap<String, String>>();

		new DataLoader().execute();

	}

	public class DataLoader extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ServiceHandler sh = new ServiceHandler();

			// Making a request to url and getting response
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

			Log.d("Response: ", "> " + jsonStr);
			if (jsonStr != null) {
				try {
					JSONArray json = new JSONArray(jsonStr);

					for (int i = 0; i < json.length(); i++) {
						JSONObject c = json.getJSONObject(i);

						String current = c.getString(TAG_CURRENT);
						String power = c.getString(TAG_POWER);
						String unit = c.getString(TAG_PRICE);
						String price = c.getString(TAG_UNIT);

						HashMap<String, String> detail = new HashMap<String, String>();

						detail.put(TAG_CURRENT, current);
						detail.put(TAG_POWER, power);
						detail.put(TAG_PRICE, price);
						detail.put(TAG_UNIT, unit);

						detailList.add(detail);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			/**
			 * Updating parsed JSON data into ListView
			 * 
			 * 
			 * */
			ListAdapter adapter = new SimpleAdapter(
					MainActivity.this,
					detailList,
					R.layout.listview_row,
					new String[] { TAG_CURRENT, TAG_POWER, TAG_PRICE, TAG_UNIT },
					new int[] { R.id.Current, R.id.Power, R.id.Unit, R.id.Price });

			setListAdapter(adapter);

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
